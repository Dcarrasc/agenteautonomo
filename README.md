# AgenteAutonomo

## Tabla de información
***
1. [Información General](#Información-general)
2. [Tecnología](#tecnologías)
3. [Ejecución del programa](#Ejecución-del-programa)
4. [Colaboracion](#Colaboraciones)
5. [Documentación](#Documentacion)
***

## Información General
En este Repositorio encontrará el Código que corresponde al desarrollo de la tarea 3 Modelamiento de un Agente Autonomo, para el ramo Seminario de Telematica II

## Tecnologías
***
* [Visual Studio Code](https://code.visualstudio.com) Version 1.61
* [GitLab](https://gitlab.com/Dcarrasc/agenteautonomo.git) Version 13.12.0
* [Python](https://www.python.org/downloads/) Version 3
* [OpenIA](https://gym.openai.com)

## Ejecución del Programa
***
Para gestionar el programa utilizamos el recurso llamado "GitLab" que con algunos conocimientos de "GIT" puedes acceder facilmente.

Para poder correr el programa primero tendrás que estar dentro de una carpeta local y hacer un **Clone** desde el repositorio Git con el siguiente link:
```
$ git clone https://gitlab.com/Dcarrasc/agenteautonomo.git
```

Luego desde tu terminal acceder a la carpeta que acabas de **"Clonar"** en este casi se creará una carpeta con el nombre "Agente Autonomo"

## Colaboraciones

Integrantes:
+ David Carrasco

## Documentacion

### Agente Autonomo
***
Trabajaremos en python uno de los problemas típicos para el aprendizaje reforzado, Tenemos un entorno de simulación, un agente que tendrá que tomar acciones, las cuales afectaran el entorno y por consecuente varía la situación del estado y el agente; y al mismo tiempo nos va a generar una señal de recompensa positiva o negativa, que ya nosotros tenemos que buscar como utilizarla para fomentar el aprendizaje

Para comenzar hay que correr el siguiente comando. 
```
pip install gym[all]
```
*Verifique que tenga la ultima versión del comando pip*

luego podemos correr el primer programa.
```
$ cd ..\JuegoBasev1.py
```

Este es un inicio básico, en donde el objetivo del agente es mantener el palo el mayor tiempo posible. En una primera instancia el movimiento del agente será aleatorio.

<p align="center">
<img src="https://gitlab.com/Dcarrasc/agenteautonomo/-/raw/main/recursos/JuegoBase1.JPG" title="Juego Base"/>
</p>

Podemos notar que el comportamiento es erratico y luego de perder sigue funcionando el entorno.

Nuestro agente en este caso es el carro negro, su espacio de acción es ir hacia la izquierda o ir hacía la derecha, siendo esta una variable discreta. (Valor 0 para izquierda, Valor 1 para derecha)

El numero que sale por pantalla es para demostrar que es aleatorio el movimiento siendo cercano a 0.5 dado las 2 opciones de movimiento que tiene.

Podemos comprobar que se va calculando en tiempo real la acción que toma, al cambiar el rango de nuestro ciclo for, así podemos dicernir de que no calcula todas las acciones antes de iniciar el programa, si no que comienza a tomar deciciones inmediatamente.


Por lo que en el segundo progama tenemos un poco más controlado el entorno y el agente, dividiendo las instancias en sesiones. Así al momento de pasar a un estado de perdida se termina la sesión y comienza otra nueva como podemos ver en el siguiente programa. 


```
$ cd ..\JuegoBasev2.py
```

Ahora podemos ver por pantalla cuantos pasos o acciones toma el agente antes de que pierda el equilibro la vara. Con esta información ya podemos establecer un máximo de tiempo que el agente puede mantener el equilibrio con movimientos aleatorios. Ahora podemos establecer una meta, para que nuestro agente superé osea tratar de maximizar la recompensa para que el agente se mantenga en este estado.

## Modelado del agente

Para ver está estapa tendremos que tener las librerias de TenserFlow instaladas en nuestro ambiente, para que nuestro agente funcione correctamente, si no tiene TenserFlow al finalizar habrá un video explicativo.

En primera instancia una vez ya definido el entorno y la meta de nuestro agente, tenemos que comenzar a preparar los datos en los que nuestro agente pueda operar, lo que se puede ver en el siguiente archivo:
```
$cd ...\JuegoBaseTrainingData.py
```
Jugamos un total de 10000 veces de forma aleatoria para almacenar los datos donde mejor le va al agente, donde la meta es mantener un puntaje de 60 minimo, luego si se cumple la condición lo guardamos en una lista llamada Training_data, para posteriormente pasarle estos datos a nuestro agente.

Por ultimo se crea la red neuronal o modelo del agente el cual podemos ver en el siguiente archivo:

```
$cd ...\JuegoBaseRedNeuronal.py
```

Video explicativo:
<p align="center">
<img src="https://gitlab.com/Dcarrasc/agenteautonomo/-/blob/main/recursos/AgenteAutonomo.mp4" />
</p>
