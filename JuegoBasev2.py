import gym
env = gym.make("CartPole-v1") #MountainCar-v1 o CartPole-v0
env.reset()

iteraciones = 100
for i_episodio in range(20):
    observation = env.reset()
    for t in range(iteraciones): #Toma acciones al azar.
        env.render()
        act = env.action_space.sample()
        obs, rew, don, inf = env.step(act)
        if don:
            print("Finalizo el episodio en {} timesteps".format(t+1))
            break
env.close
