import gym
import numpy as np


env = gym.make("CartPole-v1") #MountainCar-v1 o CartPole-v1
env.reset()
acts = []

iteraciones = 1000

for _ in range(iteraciones): #Toma acciones al azar.
    env.render()
    act = env.action_space.sample()
    acts.append(act)
    obs, rew, don, inf = env.step(act)
env.close

print(np.sum(np.array(acts)) / iteraciones)